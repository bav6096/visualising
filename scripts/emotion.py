#!/usr/bin/env python

import argparse

from visualising.communication.animation.color.rgb import RGB
from visualising.communication.animation.animation import Animation
from visualising.communication.connection import Connection
from visualising.communication.arduino import Arduino
from visualising.expression.library.raw import Raw
from visualising.expression.emotion import Emotion

parser = argparse.ArgumentParser(description="script to play an animation")
parser.add_argument("-p", "--port", help="port to which the Arduino is connected", type=str, default="/dev/ttyUSB0")
parser.add_argument("-b", "--baud", help="baud rate of the connection", type=int, default=57600)
parser.add_argument("-f", "--file", help="emotion to be played", type=str, required=True)
parser.add_argument("-t", "--time", help="time between ensembles", type=int, default=100)
args = vars(parser.parse_args())

port = args["port"]
baud = args["baud"]
file = args["file"]
time = args["time"]

color = RGB(10, 0, 0)

arduino = Arduino(Connection(port, baud))
ensembles = Raw.to_ensemble_list(file, "emotion")
animation = Animation(Emotion.build_parallel(ensembles[0], color), time, 1)
arduino.stream_animation(animation)

