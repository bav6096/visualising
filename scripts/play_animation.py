#!/usr/bin/env python

import argparse

from visualising.communication.connection import Connection
from visualising.communication.arduino import Arduino
from visualising.expression.library.raw import Raw

parser = argparse.ArgumentParser(description="script to play an animation")
parser.add_argument("-p", "--port", help="port to which the Arduino is connected", type=str, default="/dev/ttyUSB0")
parser.add_argument("-b", "--baud", help="baud rate of the connection", type=int, default=57600)
parser.add_argument("-f", "--file", help="file to be played", type=str, required=True)
parser.add_argument("-d", "--dict", help="folder containing the file to be played", type=str, required=True)
parser.add_argument("-t", "--time", help="time between ensembles", type=int, default=100)
parser.add_argument("-i", "--iter", help="number of iterations", type=int, default=1)
args = vars(parser.parse_args())

port = args["port"]
baud = args["baud"]
file = args["file"]
dict = args["dict"]
time = args["time"]
iter = args["iter"]

arduino = Arduino(Connection(port, baud))
animation = Raw.to_animation(file, dict, time, iter)
arduino.stream_animation(animation)
