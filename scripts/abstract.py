#!/usr/bin/env python

import argparse

from visualising.communication.animation.animation import Animation
from visualising.communication.connection import Connection
from visualising.communication.arduino import Arduino
from visualising.expression.abstract import Abstract

parser = argparse.ArgumentParser(description="script to play an animation")
parser.add_argument("-p", "--port", help="port to which the Arduino is connected", type=str, default="/dev/ttyUSB0")
parser.add_argument("-b", "--baud", help="baud rate of the connection", type=int, default=57600)
parser.add_argument("-t", "--time", help="time between ensembles", type=int, default=100)
args = vars(parser.parse_args())

port = args["port"]
baud = args["baud"]
delay = args["time"]

arduino = Arduino(Connection(port, baud))

name = ["A", "CPU", "A", "A", "A", "ARM", "A", "A", "A", "A", "A", "A", "A", "A",
        "A", "A", "A", "A", "RAM", "A", "CAMERA"]

error = [0, 1.0, 0.10, 0.15, 0.20, 0.85, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65,
         0.70, 0.75, 0.80, 0.25, 0.90, 0.05, 0.95]

ensemble = Abstract.cycle(error)
arduino.stream_animations(Abstract.highlight(ensemble, name, error, 4))
