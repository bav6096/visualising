#!/usr/bin/env python

import argparse

from visualising.communication.animation.animation import Animation
from visualising.communication.connection import Connection
from visualising.communication.arduino import Arduino
from visualising.expression.abstract import Abstract

parser = argparse.ArgumentParser(description="script to play an animation")
parser.add_argument("-p", "--port", help="port to which the Arduino is connected", type=str, default="/dev/ttyUSB0")
parser.add_argument("-b", "--baud", help="baud rate of the connection", type=int, default=57600)
parser.add_argument("-t", "--time", help="time between ensembles", type=int, default=1000)
parser.add_argument("-w", "--word", help="word to be displayed", type=str, required=True)
parser.add_argument("-r", "--ring", help="ring that is supposed to represent the word", type=str, default="r")
args = vars(parser.parse_args())

port = args["port"]
baud = args["baud"]
time = args["time"]
word = args["word"]
ring = args["ring"]

arduino = Arduino(Connection(port, baud))
animation = Abstract.write(word, ring)
arduino.stream_animation(Animation(animation, time, 1))
