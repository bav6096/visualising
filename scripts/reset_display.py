#!/usr/bin/env python

import argparse

from visualising.communication.connection import Connection
from visualising.communication.arduino import Arduino

parser = argparse.ArgumentParser(description="script to reset the NeoPixel rings")
parser.add_argument("-p", "--port", help="port to which the Arduino is connected", type=str, default="/dev/ttyUSB0")
parser.add_argument("-b", "--baud", help="baud rate of the connection", type=int, default=57600)
args = vars(parser.parse_args())

port = args["port"]
baud = args["baud"]

arduino = Arduino(Connection(port, baud))
arduino.reset_display()
