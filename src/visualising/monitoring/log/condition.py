#!/usr/bin/env python

class Condition:
    def __init__(self, aggregated_metrics, aggregated_domains):
        self._aggregated_metrics = aggregated_metrics
        self.aggregated_domains = aggregated_domains

    @property
    def aggregated_metrics(self):
        return self._aggregated_metrics

    @aggregated_metrics.setter
    def aggregated_metrics(self, aggregated_metrics):
        if len(aggregated_metrics) > 32:
            # Only saving the first 32 metric aggregations!
            self._aggregated_metrics = aggregated_metrics[:32]
        else:
            self._aggregated_metrics = aggregated_metrics

    def name(self):
        unzipped = [[i for i, j in self._aggregated_metrics],
                    [j for i, j in self._aggregated_metrics]]
        return unzipped[0]

    def error(self):
        unzipped = [[i for i, j in self._aggregated_metrics],
                    [j for i, j in self._aggregated_metrics]]
        return unzipped[1]
