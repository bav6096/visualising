#!/usr/bin/env python

class Metric:
    def __init__(self, origin, label, unit):
        self.origin = origin  # Origin of the monitored metric.
        self.label = label    # Label of the metric.
        self.value = None     # Value of the metric.
        self.error = None     # Error level of the metric.
        self.unit = unit      # Unit of the metric.

    def compare(self, metric):
        return self.origin == metric.origin and self.label == metric.label

    def update(self, value, error):
        self.value = value
        self.error = error
