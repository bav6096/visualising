#!/usr/bin/env python

from visualising.monitoring.log.condition import Condition


class Log:
    def __init__(self):
        self.domains = []
        self.aggregated_metrics = None
        self.aggregated_domains = None

    def add(self, domain):
        self.domains.append(domain)

    def exists(self, domain):
        for x in self.domains:
            if x.compare(domain):
                return x
        return None

    def max_error(self):
        max_error = 0.0
        for domain in self.aggregated_metrics:
            if domain[1] > max_error:
                max_error = domain[1]
        return max_error

    def min_error(self):
        min_error = 1.0
        for domain in self.aggregated_metrics:
            if domain[1] < min_error:
                min_error = domain[1]
        return min_error

    def avg_error(self):
        count = 0.001
        avg_error = 0.0
        for domain in self.aggregated_metrics:
            count = count + 1
            avg_error = avg_error + domain[1]
        return avg_error / count

    # TODO: Add more modes!
    # TODO: Think about efficiency!
    def aggregate_metrics(self, aggregation_strategy_metrics):
        aggregated_metrics = []
        for domain in self.domains:
            if domain.name in aggregation_strategy_metrics and aggregation_strategy_metrics[domain.name] != 0:

                # Strategy 1: Take the highest error level of any metric in a domain.
                if aggregation_strategy_metrics[domain] == 1:
                    aggregated_metrics.append((domain.name, domain.max_error()))

                # Strategy 2: Take the lowest error level of any metric in a domain.
                elif aggregation_strategy_metrics[domain] == 2:
                    aggregated_metrics.append((domain.name, domain.min_error()))

                else:
                    raise ValueError("The specified method for aggregating metrics is not recognised!")

            # Default strategy: Take the average error level of every metric in a domain.
            else:
                aggregated_metrics.append((domain.name, domain.avg_error()))

        self.aggregated_metrics = aggregated_metrics

    # TODO: Add more modes!
    def aggregate_domains(self, aggregation_strategy_domains):
        # Strategy 1: Take the highest error level of any domain.
        if aggregation_strategy_domains == 1:
            self.aggregated_domains = self.max_error()

        # Strategy 2: Take the lowes error level of any domain.
        elif aggregation_strategy_domains == 2:
            self.aggregated_domains = self.min_error()

        # Default strategy: Take the average error level of every domain.
        else:
            self.aggregated_domains = self.avg_error()

    def condition(self, aggregation_strategy_metrics, aggregation_strategy_domains):
        self.aggregate_metrics(aggregation_strategy_metrics)
        self.aggregate_domains(aggregation_strategy_domains)
        return Condition(self.aggregated_metrics, self.aggregated_domains)
