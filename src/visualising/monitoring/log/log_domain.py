#!/usr/bin/env python

class Domain:
    def __init__(self, name):
        self.name = name
        self.metrics = []

    def compare(self, domain):
        return self.name == domain.name

    def add(self, metric):
        self.metrics.append(metric)

    def exists(self, metric):
        for x in self.metrics:
            if x.compare(metric):
                return x
        return None

    def max_error(self):
        max_error = 0.0
        for metric in self.metrics:
            if metric.error > max_error:
                max_error = metric.error
        return max_error

    def min_error(self):
        min_error = 1.0
        for metric in self.metrics:
            if metric.error < min_error:
                min_error = metric.error
        return min_error

    def avg_error(self):
        count = 0.001
        avg_error = 0.0
        for metric in self.metrics:
            count = count + 1
            avg_error = avg_error + metric.error
        return avg_error / count
