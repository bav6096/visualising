#!/usr/bin/env python

import rospy

from monitoring.msg import Monitoring
from visualising.monitoring.log.log_metric import Metric
from visualising.monitoring.log.log_domain import Domain
from visualising.monitoring.log.log import Log


class Watchdog:
    def __init__(self):
        self.sub = rospy.Subscriber("/monitoring", Monitoring, self.update_log)

        # Stores the most current values of each metric.
        self.log = Log()

        # Strategies that specify how the critical level of each metric should be aggregated in a domain.
        # It is possible to specify a different aggregation strategy for every single domain.
        self.aggregation_strategy_metrics = rospy.get_param("/visualiser/aggregation_strategy_metrics")

        # Strategy that specifies how the critical levels of all domains should be aggregated.
        self.aggregation_strategy_domains = rospy.get_param("/visualiser/aggregation_strategy_domains")

    def update_log(self, monitoring):
        new_metric = Metric(monitoring.origin, monitoring.metric.label, monitoring.metric.unit)
        new_metric.update(monitoring.metric.value, monitoring.metric.error)

        new_domain = Domain(monitoring.metric.domain)
        new_domain.add(new_metric)

        domain = self.log.exists(new_domain)
        if domain is None:
            self.log.add(new_domain)
        else:
            metric = domain.exists(new_metric)
            if metric is None:
                domain.add(new_metric)
            else:
                metric.update(monitoring.metric.value, monitoring.metric.error)

    def condition(self):
        return self.log.condition(self.aggregation_strategy_metrics, self.aggregation_strategy_domains)
