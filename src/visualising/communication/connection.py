#!/usr/bin/env python

import time
import serial

from visualising.communication.message.response import Response


class ArduinoException(Exception):
    def __init__(self, desc):
        self.desc = desc


class Connection:
    def __init__(self, port, baud):
        self.connection = serial.Serial()
        self.connection.port = port
        self.connection.baudrate = baud
        self.connection.timeout = 1

        self.connection.open()  # Establishes a serial connection.

        time.sleep(2)  # Prevents errors regarding pyserial.

    # Send a message to the Arduino.
    def send_msg(self, msg):
        for byte in msg.export_message():
            self.connection.write(byte)

        self.connection.reset_output_buffer()  # Clear serial output buffer.

    # Receive a message from the Arduino.
    def receive_msg(self):
        try:
            # Since the timeout is set to one second, there is a waiting period of
            # exactly one second to read a byte from the serial buffer. If no byte
            # is read in this time period, an exception is thrown.
            byte = self.connection.read(1)
        except SerialException:  # Arduino has not responded in time.
            byte = b'\xff'

        self.connection.reset_input_buffer()  # Clear serial input buffer.

        return Response(byte)

    # Send a message and confirm that the response is as expected.
    # If the response is not as expected, try again as often as
    # specified. In case any response wasn't even once one of the
    # expected responses raise an exception.
    def confirm_msg(self, msg, expectations, resends):
        if not len(expectations) > 0:
            raise ValueError("The parameter expectations must be a lister with objects of the class Response, "
                             "the length of which must be greater than 1!")
        if not resends > 0:
            raise ValueError("The parameter resends must be an integer greater 0!")

        def evaluate():
            boolean = resends > 0
            for expectation in expectations:
                boolean = boolean and not response.compare(expectation)
            return boolean

        while True:
            self.send_msg(msg)
            response = self.receive_msg()
            if not evaluate():
                break
            resends = resends - 1

        if not resends > 0:
            raise ArduinoException(response.desc)
