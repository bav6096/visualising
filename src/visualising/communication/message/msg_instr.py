#!/usr/bin/env python

from visualising.communication.message.message import Message


class MsgInstr(Message):
    def __init__(self, instr, animation=None):
        self._instr = instr
        self.animation = animation

    @property
    def instr(self):
        return self._instr

    @instr.setter
    def instr(self, instr):
        if instr != "A" and instr != "B" and instr != "C" and instr != "D" and instr != "E":
            raise ValueError("Unknown instruction!")
        self._instr = instr

    # Creates a 50-bit instruction message. Since only 7 bits at most are required for the
    # actual message, most of the message consists only of zeros. The first and last bit
    # are intended for authentication as an instruction message.
    def export_message(self):
        message = [bytes('[', 'ascii'), bytes(self.instr, 'ascii')]

        if self.animation is not None:
            num_frames = len(self.animation.ensembles) * 2
            message.append(num_frames.to_bytes(1, byteorder='big'))

            ensemble_time_bytes = self.animation.ensemble_time.to_bytes(4, byteorder='big')
            message.append((ensemble_time_bytes[0]).to_bytes(1, byteorder='big'))
            message.append((ensemble_time_bytes[1]).to_bytes(1, byteorder='big'))
            message.append((ensemble_time_bytes[2]).to_bytes(1, byteorder='big'))
            message.append((ensemble_time_bytes[3]).to_bytes(1, byteorder='big'))

            message.append(self.animation.num_iter.to_bytes(1, byteorder='big'))

            for _ in range(41):
                message.append((0).to_bytes(1, byteorder='big'))
        else:
            for _ in range(47):
                message.append((0).to_bytes(1, byteorder='big'))

        message.append(bytes(']', 'ascii'))

        return message
