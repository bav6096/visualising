#!/usr/bin/env python

from visualising.communication.message.message import Message


class MsgFrame(Message):
    def __init__(self, frame):
        self.frame = frame

    # Creates a 50-bit frame message. The first and last bit are intended for authentication
    # as a frame message. The remaining 48 bits store exactly one frame.
    def export_message(self):
        message = [bytes('{', 'ascii')]

        for pixel in self.frame.pixels:
            r = pixel.color.r
            g = pixel.color.g
            b = pixel.color.b

            message.append(r.to_bytes(1, byteorder='big'))
            message.append(g.to_bytes(1, byteorder='big'))
            message.append(b.to_bytes(1, byteorder='big'))

        message.append(bytes('}', 'ascii'))

        return message
