#!/usr/bin/env python

from abc import ABC, abstractmethod


class Message(ABC):
    @abstractmethod
    def export_message(self):
        pass
