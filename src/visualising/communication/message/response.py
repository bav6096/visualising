#!/usr/bin/env python

class Response:
    # Generates an Arduino message, which is composed of a message byte from the Arduino
    # and a translation of the message byte.
    def __init__(self, byte):
        self._byte = byte
        self.desc = self.translate()

    @property
    def byte(self):
        return self._byte

    @byte.setter
    def byte(self, byte):
        self._byte = byte

    def translate(self):
        translation = {
            240: "Animation has to many frame ensembles!",  # 0xF0
            241: "No entities is loaded!",  # 0xF1
            242: "Instruction was not recognized!",  # 0xF2
            243: "No entities message expected!",  # 0xF3
            244: "Message has wrong format!",  # 0xF4
            245: "Animation is playing!",  # 0xF5
            246: "Animation has no iteration!",  # 0xF6
            247: "Unequal number of frames_library for the left and right NeoPixel ring!",  # 0xF7
            248: "Animation has no frame ensemble!",  # 0xF8
            255: "Timeout!",  # 0xFF
            0: "No response!",  # 0x00
            15: "Waiting for frames_library to be send!",  # 0x0F
            31: "Animation playback has been started!",  # 0x1F
            47: "Frame successfully received!",  # 0x2F
            63: "Last frame successfully received!",  # 0x3F
            79: "Animation successfully played!",  # 0x4F
            95: "Animation playback has been paused!",  # 0x5F
            111: "Animation playback has been resumed!",  # 0x6F
            127: "Displays have been cleared!",  # 0x7F
        }

        try:
            return translation[int.from_bytes(self.byte, byteorder='big')]
        except KeyError:
            return "Unknown response!"

    def compare(self, response):
        return bytes(self.byte) == bytes(response.byte)
