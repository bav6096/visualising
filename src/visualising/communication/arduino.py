#!/usr/bin/env python

import time

from visualising.communication.animation.animation import Animation
from visualising.communication.message.msg_frame import MsgFrame
from visualising.communication.message.msg_instr import MsgInstr
from visualising.communication.message.response import Response


class Arduino:
    def __init__(self, connection):
        self.connection = connection

    # Creates instruction message and sends it to the Arduino,
    # to play a loaded animation.
    def start_playback(self):
        self.connection.confirm_msg(MsgInstr('B'), [Response(b'\x1f')], 5)

    # Creates instruction message and sends it to the Arduino,
    # to pause a playing animation.
    def pause_playback(self):
        self.connection.confirm_msg(MsgInstr('C'), [Response(b'\x5f')], 5)

    # Creates instruction message and sends it to the Arduino,
    # to resume playback of a loaded animation.
    def resume_playback(self):
        self.connection.confirm_msg(MsgInstr('D'), [Response(b'\x6f')], 5)

    # Creates instruction message and sends it to the Arduino,
    # to clear the NeoPixel rings.
    def reset_display(self):
        self.connection.confirm_msg(MsgInstr('E'), [Response(b'\x7f')], 5)

    # Creates one instruction messages and multiple frame messages to load an animation
    # onto the Arduino.
    def load_animation(self, animation):
        if len(animation.ensembles) > 16:
            raise ValueError("The animation parameter must be an object of the Animation class, "
                             "which may contain a maximum of 16 ensembles!")

        self.connection.confirm_msg(MsgInstr('A', animation), [Response(b'\x0f')], 5)

        for ensemble in animation.ensembles:
            l_frame = ensemble.l_frame
            r_frame = ensemble.r_frame

            self.connection.confirm_msg(MsgFrame(l_frame), [Response(b'\x2f'), Response(b'\x3f')], 10)
            self.connection.confirm_msg(MsgFrame(r_frame), [Response(b'\x2f'), Response(b'\x3f')], 10)

    # First, an animation is loaded onto the Arduino. The playback of this
    # animation is then started. In addition, it is checked whether the
    # animation was played successfully.
    def play_animation(self, animation):
        self.load_animation(animation)
        self.start_playback()

        # Estimated playback time in seconds.
        playback_time = animation.ensemble_time * len(animation.ensembles) * 0.001
        # Current time in seconds.
        curr_time = time.time()

        while not Response(b'\x4f').compare(self.connection.receive_msg()):
            # Wait 10 times the estimated animation playback time for a response.
            if time.time() > curr_time + playback_time * 10:
                raise ArduinoException("Successful playback of animation can't be confirmed!")

    # Streams an animation to the Arduino. For this purpose, one ensemble of
    # the animation is repeatedly sent to the Arduino. The Arduino plays
    # the received ensemble and then confirms the playback, whereupon a new
    # ensemble is sent.
    def stream_animation(self, animation):
        for _ in range(animation.num_iter):
            for ensemble in animation.ensembles:
                self.play_animation(Animation([ensemble], animation.ensemble_time, 1))

    # Streams multiple animations in succession to the Arduino. This functionality
    # makes it possible to send individual ensembles as animations and thus to play
    # back individual ensembles with individual ensemble time.
    def stream_animations(self, animations):
        for animation in animations:
            self.stream_animation(animation)
