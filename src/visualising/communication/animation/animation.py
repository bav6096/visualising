#!/usr/bin/env python


class Animation:
    # The ensemble time is given in milliseconds.
    def __init__(self, ensembles, ensemble_time, num_iter):
        self.ensembles = ensembles
        self._ensemble_time = ensemble_time
        self._num_iter = num_iter

    @property
    def ensemble_time(self):
        return self._ensemble_time

    @ensemble_time.setter
    def ensemble_time(self, ensemble_time):
        if not 0 <= ensemble_time <= 4294967295:
            raise ValueError("The parameter ensemble_time  must be an integer between 0 an 4294967295!")
        self._ensemble_time = ensemble_time

    @property
    def num_iter(self):
        return self._num_iter

    @num_iter.setter
    def num_iter(self, num_iter):
        if not 1 <= num_iter <= 255:
            raise ValueError("The parameter num_iter must be an integer between 1 an 255!")
        self._num_iter = num_iter
