#!/usr/bin/env python

from visualising.communication.animation.color.color import Color


class RGB(Color):
    def __init__(self, r, g, b):
        self._r = r
        self._g = g
        self._b = b

    @property
    def r(self):
        return self._r

    @r.setter
    def r(self, r):
        if not 0 <= r <= 255:
            raise ValueError("The parameter r must be an integer between 0 and 255!")
        self._r = r

    @property
    def g(self):
        return self._g

    @g.setter
    def g(self, g):
        if not 0 <= g <= 255:
            raise ValueError("The parameter g must be an integer between 0 and 255!")
        self._g = g

    @property
    def b(self):
        return self._b

    @b.setter
    def b(self, b):
        if not 0 <= b <= 255:
            raise ValueError("The parameter b must be an integer between 0 and 255!")
        self._b = b

    @property
    def illuminated(self):
        return self._r + self._g + self._b != 0

    def export_color(self):
        return [self._r, self._g, self._b]
