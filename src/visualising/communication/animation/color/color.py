#!/usr/bin/env python

from abc import ABC, abstractmethod


class Color(ABC):
    @property
    @abstractmethod
    def r(self):
        pass

    @property
    @abstractmethod
    def g(self):
        pass

    @property
    @abstractmethod
    def b(self):
        pass

    @property
    @abstractmethod
    def illuminated(self):
        pass

    @abstractmethod
    def export_color(self):
        pass
