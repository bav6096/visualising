#!/usr/bin/env python

from visualising.communication.animation.color.rgb import RGB
from visualising.communication.animation.pixel import Pixel
from visualising.communication.animation.frame import Frame


class Ensemble:
    def __init__(self, l_frame, r_frame):
        self.l_frame = l_frame
        self.r_frame = r_frame

    def color(self, index):
        if not 0 <= index <= 31:
            raise ValueError("The parameter index must be an integer between 0 and 31!")
        if index < 16:
            return self.l_frame.color(index)
        else:
            return self.r_frame.color(index - 16)

    def illuminated(self, index):
        if not 0 <= index <= 31:
            raise ValueError("The parameter index must be an integer between 0 and 31!")
        if index < 16:
            return self.l_frame.illuminated(index)
        else:
            return self.r_frame.illuminated(index - 16)

    def replace_pixel(self, index, pixel):
        if not 0 <= index <= 31:
            raise ValueError("The parameter index must be an integer between 0 and 31!")
        if index < 16:
            self.l_frame.replace_pixel(index, pixel)
        else:
            self.r_frame.replace_pixel(index - 16, pixel)

    @staticmethod
    def empty():
        l_pixels = []
        r_pixels = []
        for _ in range(16):
            l_pixels.append(Pixel(RGB(0, 0, 0)))
            r_pixels.append(Pixel(RGB(0, 0, 0)))

        return Ensemble(Frame(l_pixels), Frame(r_pixels))
