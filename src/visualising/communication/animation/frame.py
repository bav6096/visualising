#!/usr/bin/env python

class Frame:
    def __init__(self, pixels):
        self._pixels = pixels

    @property
    def pixels(self):
        return self._pixels

    @pixels.setter
    def pixels(self, pixels):
        if len(pixels) != 16:
            raise ValueError("The parameter pixels must be a list of exactly 16 objects of the Pixel class!")
        self._pixels = pixels

    def color(self, index):
        if not 0 <= index <= 15:
            raise ValueError("The parameter index must be an integer between 0 and 15!")
        return self.pixels[index].color

    def illuminated(self, index):
        if not 0 <= index <= 15:
            raise ValueError("The parameter index must be an integer between 0 and 15!")
        return self.pixels[index].color.illuminated

    def replace_pixel(self, index, pixel):
        if not 0 <= index <= 15:
            raise ValueError("The parameter index must be an integer between 0 and 15!")
        self.pixels[index] = pixel
