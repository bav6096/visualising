#!/usr/bin/env python

import rospy

from visualising.communication.connection import Connection
from visualising.communication.arduino import Arduino
from visualising.expression.emotion import Emotion
from visualising.expression.abstract import Abstract
from visualising.monitoring.watchdog import Watchdog


class Visualiser:
    def __init__(self):
        port = rospy.get_param("/visualiser/arduino_port")
        baud = rospy.get_param("/visualiser/arduino_baud")
        freq = rospy.get_param("/visualiser/vis_freq")

        arduino = Arduino(Connection(port, baud))
        self.watchdog = Watchdog()

        if rospy.get_param("/visualiser/visualisation_strategy") == 1:
            self.expression = Emotion(arduino)
        else:
            self.expression = Abstract(arduino)

        if not freq > 0.0:
            rospy.logwarn(
                "Visualiser: The frequency at which the visualisation of the robot state is updated must be greater "
                "than 0! Using 1 as frequency!")
            freq = 1.0

        self.timer = rospy.Timer(rospy.Duration(1.0 / freq), self.visualise)

    def visualise(self, _):
        self.expression.react(self.watchdog.condition())
