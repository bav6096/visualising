#!/usr/bin/env python

import re
import importlib.resources

from visualising.communication.animation.color.rgb import RGB
from visualising.communication.animation.pixel import Pixel
from visualising.communication.animation.frame import Frame
from visualising.communication.animation.ensemble import Ensemble
from visualising.communication.animation.animation import Animation
from visualising.expression.library import alphabet
from visualising.expression.library import animation
from visualising.expression.library import emotion
from visualising.expression.library import transition


class Raw:
    def __init__(self):
        pass

    @staticmethod
    def to_ensemble_list(filename, directory):
        def load():
            if directory != "emotion" and directory != "alphabet" and directory != "animation" \
                    and directory != "transition":
                raise ValueError("The parameter directory describes a unknown directory!")
            if directory == "emotion":
                return importlib.resources.open_text(emotion, filename)
            if directory == "alphabet":
                return importlib.resources.open_text(alphabet, filename)
            if directory == "animation":
                return importlib.resources.open_text(animation, filename)
            if directory == "transition":
                return importlib.resources.open_text(transition, filename)

        def to_pixel_list(line):
            pixels = []
            i, r, g, b = 0, 0, 0, 0
            for value in line.split(","):
                if i % 3 == 0:
                    r = int(re.search(r'\d+', value).group())
                if i % 3 == 1:
                    g = int(re.search(r'\d+', value).group())
                if i % 3 == 2:
                    b = int(re.search(r'\d+', value).group())
                    pixels.append(Pixel(RGB(r, g, b)))
                i = i + 1
            return pixels

        file = load()
        ensembles = []
        while True:
            l_line = file.readline()
            r_line = file.readline()

            if bool(l_line) != bool(r_line):
                raise ValueError("Animation must have an even number of frames!")
            if not l_line and not r_line:
                break

            l_frame = Frame(to_pixel_list(l_line))
            r_frame = Frame(to_pixel_list(r_line))

            ensembles.append(Ensemble(l_frame, r_frame))

        return ensembles

    @staticmethod
    def to_animation(filename, directory, ensemble_time, num_iter):
        ensemble_list = Raw.to_ensemble_list(filename, directory)
        return Animation(ensemble_list, ensemble_time, num_iter)
