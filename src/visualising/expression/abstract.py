#!/usr/bin/env python

import time
import rospy
import numpy as np

from visualising.communication.animation.color.rgb import RGB
from visualising.communication.animation.pixel import Pixel
from visualising.communication.animation.ensemble import Ensemble
from visualising.communication.animation.animation import Animation
from visualising.expression.library.raw import Raw
from visualising.expression.expression import Expression
from visualising.expression.tool import Tool


class Abstract(Expression):
    def __init__(self, arduino):
        self._arduino = arduino
        self.displayed = True

    @property
    def arduino(self):
        return self._arduino

    @arduino.setter
    def arduino(self, arduino):
        self._arduino = arduino

    def react(self, condition):
        if self.displayed:
            self.displayed = False

            animations = Abstract.highlight(Abstract.cycle(condition.error()), condition.name(), condition.error(), 5)

            self.arduino.stream_animations(animations)
            self.displayed = True

    @staticmethod
    def cycle(error_vector):
        empty = Ensemble.empty()
        index = 0
        for error_value in error_vector:
            color = Tool.value_to_color(error_value, RGB(0, 50, 0), RGB(50, 0, 0))
            empty.replace_pixel(index, Pixel(color))
            index = index + 1
        return empty

    @staticmethod
    def write(text, ring, index=None, color=None):
        if not text.isalpha():
            raise ValueError("The parameter text must be a string that only contains alphabetic characters!")

        text = text.upper()

        ensembles = []
        for char in text:
            if ring == "l":
                filename = "l_" + char + ".txt"
            elif ring == "r":
                filename = "r_" + char + ".txt"
            else:
                raise ValueError("The parameter ring must be a character that is either l or r!")

            letter_ensemble = Raw.to_ensemble_list(filename, "alphabet")
            if index is not None:
                letter_ensemble[0].replace_pixel(index, Pixel(color))
            ensembles = ensembles + letter_ensemble
            if index is not None:
                empty = Ensemble.empty()
                empty.replace_pixel(index, Pixel(color))
                ensembles.append(empty)
            else:
                ensembles.append(Ensemble.empty())
        return ensembles

    @staticmethod
    def highlight(generated_cycle, name_vector, error_vector, number):
        number = min(len(name_vector), number)
        zipped = sorted(zip(error_vector, range(len(error_vector))), reverse=True)[:number]

        cycle_animation = Animation([generated_cycle], 5000, 1)

        animations = [cycle_animation]
        for tuple in zipped:
            highlighted_ring = ""
            highlighted_color = None
            highlighted_index = None

            empty = Ensemble.empty()
            ensembles = []
            for i in range(32):
                if tuple[1] != i:
                    empty.replace_pixel(i, Pixel(RGB(0, 0, 0)))
                else:
                    highlighted_index = i
                    if i < 16:
                        highlighted_ring = "l"
                    else:
                        highlighted_ring = "r"

                    highlighted_color = generated_cycle.color(i)
                    empty.replace_pixel(i, Pixel(highlighted_color))

            ensembles.append(empty)
            ensembles = ensembles + Abstract.write(name_vector[highlighted_index], highlighted_ring, highlighted_index,
                                                   highlighted_color)
            animations.append(Animation(ensembles, 1000, 1))
            animations.append(cycle_animation)

        animations.append(Animation([Ensemble.empty()], 1000, 1))

        return animations
