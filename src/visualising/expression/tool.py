#!/usr/bin/env python

import numpy as np

from visualising.communication.animation.color.rgb import RGB
from visualising.communication.animation.pixel import Pixel


class Tool:
    def __init__(self):
        pass

    @staticmethod
    def value_to_color(value, start_color, end_color):
        if not 0 <= value <= 1:
            raise ValueError("The parameter value must be a floating point number between 0 and 1!")

        color = (1 - value) * np.array(start_color.export_color()) + value * np.array(end_color.export_color())
        color = color.tolist()

        return RGB(int(color[0]), int(color[1]), int(color[2]))

    @staticmethod
    def color_ensemble(ensemble, color):
        for i in range(32):
            if ensemble.illuminated(i):
                ensemble.replace_pixel(i, Pixel(color))
        return ensemble
