#!/usr/bin/env python

from abc import ABC, abstractmethod


class Expression(ABC):
    @property
    @abstractmethod
    def arduino(self):
        pass

    @abstractmethod
    def react(self, state):
        pass
