#!/usr/bin/env python

import time

from visualising.communication.animation.color.rgb import RGB
from visualising.communication.animation.pixel import Pixel
from visualising.communication.animation.ensemble import Ensemble
from visualising.communication.animation.animation import Animation
from visualising.expression.library.raw import Raw
from visualising.expression.expression import Expression
from visualising.expression.tool import Tool


class Emotion(Expression):
    def __init__(self, arduino):
        self._arduino = arduino
        self.displayed = True

        self.happy = "emotion_happy.txt"
        self.ok = "emotion_ok.txt"
        self.angry = "emotion_angry.txt"

    @property
    def arduino(self):
        return self._arduino

    @arduino.setter
    def arduino(self, arduino):
        self._arduino = arduino

    def react(self, condition):
        aggregated_domains = condition.aggregated_domains
        if self.displayed:
            self.displayed = False
            color = Tool.value_to_color(aggregated_domains, RGB(25, 25, 25), RGB(25, 0, 0))
            if aggregated_domains > 0.7:
                self.visualise(self.angry, color)
            else:
                if aggregated_domains > 0.3:
                    self.visualise(self.ok, color)
                else:
                    self.visualise(self.happy, color)

    def visualise(self, filepath, color):
        still = Raw.to_ensemble_list(filepath, "emotion")
        buildup = Emotion.build_parallel(Tool.color_ensemble(still[0], color), color)
        animation = Animation(buildup, 50, 1)

        self.arduino.stream_animation(animation)
        self.displayed = True

    # The function is given an ensemble. Then an animation is generated that builds
    # up the given ensemble.
    @staticmethod
    def build(still, color):
        ensembles = []
        for i in range(32 + 1):
            empty = Ensemble.empty()
            if i < 32:
                for j in range(i + 1):
                    if still.illuminated(j) or j == i:
                        empty.replace_pixel(j, Pixel(color))
                ensembles.append(empty)
            else:
                ensembles.append(still)
        ensembles.append(still)
        return ensembles

    # The function is given an ensemble. Then an animation is generated that builds
    # up the given ensemble. The generated animation builds up the given ensemble
    # on both NeoPixel rings in parallel.
    @staticmethod
    def build_parallel(still, color):
        ensembles = []
        for i in range(16 + 1):
            empty = Ensemble.empty()
            if i < 16:
                for j in range(i + 1):
                    if still.illuminated(j):
                        empty.replace_pixel(j, Pixel(color))
                    if still.illuminated(j + 16):
                        empty.replace_pixel(j + 16, Pixel(color))
                    if j == i:
                        empty.replace_pixel(j, Pixel(color))
                        empty.replace_pixel(j + 16, Pixel(color))
                ensembles.append(empty)
            else:
                ensembles.append(still)
        ensembles.append(still)
        return ensembles
